package semaforo;

public class Semaforo {

	int timer = 50, timerAttuale = 0;
	String colore = "rosso";
	
	public Semaforo(int delay) {
		this.timer = delay;
	}
	
	public void start() {
		while (true) {
		    if (timerAttuale < 0) {
		    	change();
		    	System.out.println(this);
		        timerAttuale = timer;
		    }
		    timerAttuale--;
		}
	}
	
	private void change() {
		switch(colore) {
		case "rosso":
		default:
			colore = "verde";
			break;
		case "giallo":
			colore = "rosso";
			break;
		case "verde":
			colore = "giallo";
			break;
		}
	}
	
	public String toString() {
		return "colore: " + colore;
	}
	
}
